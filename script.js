const img = document.querySelector("img");

setInterval(() => {
  img.style.transform = `scale(${Math.random()})`;
}, 1000);